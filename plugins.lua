local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.vim/plugged')
  -- Color scheme
  Plug 'ellisonleao/gruvbox.nvim'

  -- Completion
  Plug "hrsh7th/nvim-cmp" -- The completion plugin
  Plug "hrsh7th/cmp-buffer" -- buffer completions
  Plug "hrsh7th/cmp-path" -- path completions
  Plug "hrsh7th/cmp-cmdline" -- cmdline completions
  Plug "saadparwaiz1/cmp_luasnip" -- snippet completions
  Plug "hrsh7th/cmp-nvim-lsp"
  Plug "hrsh7th/cmp-nvim-lua"

  -- Telescope
  Plug "nvim-telescope/telescope.nvim"
  Plug 'nvim-telescope/telescope-media-files.nvim'
  Plug 'nvim-telescope/telescope-fzf-native.nvim'
  Plug 'nvim-lua/plenary.nvim'
  Plug 'BurntSushi/ripgrep'

  -- Snippets
  Plug "L3MON4D3/LuaSnip" --snippet engine
  Plug "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- Lsp
  Plug "neovim/nvim-lspconfig" -- enable LSP
  Plug "williamboman/nvim-lsp-installer" -- simple to use language server installer

  -- Treesitter
  Plug "nvim-treesitter/nvim-treesitter"

  -- Bufferline
  Plug "kyazdani42/nvim-web-devicons"
  Plug "akinsho/bufferline.nvim"

  -- Lualine
  Plug "nvim-lualine/lualine.nvim"

  -- Nvim-cheat
  Plug "RishabhRD/nvim-cheat.sh"
  Plug "RishabhRD/popfix"

  -- Git
  Plug "lewis6991/gitsigns.nvim"

  -- Toggleterm
  Plug "akinsho/toggleterm.nvim"

  -- Latex
  Plug 'lervag/vimtex'
vim.call('plug#end')
